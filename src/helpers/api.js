import axios from 'axios';
import { ToastProgrammatic as Toast } from 'buefy';

const BASE_URL = 'http://localhost:8000/api';
const JWT_KEY = 'EDT_JWT';

const api = axios.create({
  baseURL: BASE_URL,
});

api.interceptors.request.use(config => {
  const token = localStorage.getItem(JWT_KEY);
  config.headers.Authorization = token ? `Bearer ${token}` : null;
  return config;
});

function logApiError(error) {
  const {
    response: { data },
  } = error;
  if (data.code === 401) {
    Toast.open({
      message: 'Non autorisé à effectuer cette action.',
      type: 'is-danger',
      queue: false,
    });
  } else {
    Object.keys(data).forEach(field => {
      const val = data[field];
      if (Array.isArray(val)) {
        val.forEach(message =>
          Toast.open({ message, type: 'is-danger', queue: false })
        );
      } else {
        Toast.open({ message: val, type: 'is-danger', queue: false });
      }
    });
  }
}

export { logApiError, JWT_KEY };
export default api;
