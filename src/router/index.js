import VueRouter from 'vue-router';

// Pages
import NoteProf from '../pages/NoteProf';
import Edt from '../pages/Edt';
import Error404 from '../pages/Error404';

const routes = [
  { path: '/', component: NoteProf, name: 'note-prof', label: 'Note ton prof' },
  { path: '/edt', component: Edt, name: 'edt', label: 'Emploi du temps' },
];

const router = new VueRouter({
  routes: routes.concat({ path: '*', component: Error404 }),
  mode: 'history',
});

export default router;

export { routes };
