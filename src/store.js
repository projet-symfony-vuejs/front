export default {
  state: {
    email: '',
  },
  mutations: {
    setEmail(state, newEmail) {
      state.email = newEmail;
    },
    clearEmail(state) {
      state.email = '';
    },
  },
};
