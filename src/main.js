// Node packages
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';

import App from './App';
import router from './router';

// Global components to register
import TeacherCard from './components/TeacherCard';
import FormAvis from './components/FormAvis';
import NavigationBar from './components/NavigationBar';

// Vuex Store
import store from './store';

Vue.use(VueRouter);
Vue.use(Buefy);
Vue.use(Vuex);
Vue.component('TeacherCard', TeacherCard);
Vue.component('FormAvis', FormAvis);
Vue.component('NavigationBar', NavigationBar);
Vue.config.productionTip = false;

new Vue({
  router,
  store: new Vuex.Store(store),
  render: h => h(App),
}).$mount('#app');
